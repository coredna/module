<?php

use module_name\XXX;

/**
 * Xxx -> name of item for function
 * xxx -> lowercase item
 * module -> this module
 */

/**
 * @param array $args
 * @return array
 */
function module_GetXxxs(array $args = [])
{
    $results = [];
    if (empty($args['centre_id'])) {
        $args['centre_id'] = $_SESSION['centre_id'];
    }
    $records = module_userapi_get_xxxs($args);
    if(!empty($records)) {
        foreach($records as $record) {
            $results[] = new Xxx($record);
        }
    }
    return $results;
}

/**
 * @param array $args
 * @return bool
 */
function module_AddXxx(array $args = [])
{
    if (empty($args['centre_id'])) {
        $args['centre_id'] = $_SESSION['centre_id'];
    }
    return module_userapi_add_xxx($args);
}

/**
 * @param array $args
 * @return bool
 */
function module_UpdateXxx(array $args = [])
{
    if (empty($args['centre_id'])) {
        $args['centre_id'] = $_SESSION['centre_id'];
    }
    return module_userapi_update_xxx($args);
}

/**
 * @param array $args
 * @return bool
 */
function module_DeleteXxx(array $args = [])
{
    if (empty($args['centre_id'])) {
        $args['centre_id'] = $_SESSION['centre_id'];
    }
    return module_userapi_delete_xxx($args);
}

/**
 * @param $id
 * @return bool
 * @throws Exception
 */
function module_GetXxxById($id)
{
    $args['id'] = (int) $id;
    if (empty($args['centre_id'])) {
        $args['centre_id'] = $_SESSION['centre_id'];
    }
    if (!is_numeric($id)) {
        throw new Exception("Id: {$id} is not a number");
    }
    $record = current(module_userapi_get_xxxs($args));

    if(!empty($record)) {
        return new Xxx($record);
    } else {
        return false;
    }
}
