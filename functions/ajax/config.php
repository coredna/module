<?php

/**
 * @param $div
 * @param array $args
 * @param $id
 * @param string $form_name
 * @param string $form_action
 * @param string $start
 * @param string $parent_id
 * @return mixed
 */
function xxx_config($div, $args = array(), $id, $form_name = 'config', $form_action = '', $start = '', $parent_id = '')
{
    global $objResponse, $settings, $template;

    if (trim($form_name) == '') {
        $form_name = 'config';
    }

    $template->assign('form_name', $form_name);
    $template->assign('form_action', $form_name);

    $params = array('use_inheritance');
    $module = 'module';
    $centre_id = $_SESSION['centre_id'];

    switch (trim($form_name)) {
        case 'config_update':
            break;
    }

    $template->assign('modules_path', $settings->_AppPath . $settings->_AdminDir . 'modules/');
    $objResponse->assign($div, 'innerHTML', $template->fetch($settings->_AppPath . $settings->_AdminDir . 'modules/module/templates/ajax/config.html'));

    $current_module = api_call('modules', 'GetModuleByAction', 'module');
    $module_name = ($current_module ? $current_module->getName() : 'module');

    $objResponse->assign('syshelpcat_name', 'value', $module_name . '/Configuration');

    $objResponse->script('showHelpToggle()');
    $objResponse->script('initPublishTab();');

    return $objResponse;
}
