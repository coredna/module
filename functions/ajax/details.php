<?php

/**
 *
 *
 * @param array  $args
 * @param int    $id
 * @param string $action
 *
 * @return mixed
 *
 * @throws Exception
 */
function module_xxxs($args = [], $id = 0, $action = 'list')
{
    global $template, $settings, $objResponse, $user;

    $modulesPath = $settings->_AppPath . $settings->_AdminDir . 'modules';
    $DS = DIRECTORY_SEPARATOR;
    $ajaxTemplatesPath = $modulesPath . $DS . 'module' . $DS . 'templates' . $DS . 'ajax' . $DS;
    $objResponse->assign('error_msg_new', 'value', '');
    $objResponse->assign('success_msg_new', 'value', '');
    $objResponse->assign('error_msg', 'value', '');
    $objResponse->assign('success_msg', 'value', '');
    $listTemplate = 'xxxs.html';
    $editTemplate = 'xxx.html';
    $templateToUse = $listTemplate;
    $xxxs = [];
    $xxx = new Xxx([
        'id' => 0,
        'centre_id' => getCentreId()
    ]);
    // get the xxx
    if ($id) {
        $xxx = module_GetXxxById($id);
    }
    $error = 'There was an error, Please try again.';
    $div = 'details';
    $success = null;
    $result = null;
    // always use the current centre
    $args['centre_id'] = getCentreId();

    // 'list' is the default form_name
    switch (trim($action)) {
        default:
            // listing is done below as some action here need to list afterwards
            $action = 'list';
            break;
        case 'publish':
            break;
        case 'edit':
            $div = 'pop_window';
            $templateToUse = $editTemplate;
            break;
        case 'save':
            // validation
            $validation = [];
            if (empty($args['name'])) {
                $validation[] = 'You must provide a name';
            }
            if ($validation) {
                // pass along the args
                $xxx->setData($args);
                $div = 'pop_window';
                $result = false;
                $templateToUse = $editTemplate;
                $error = implode('<br>', $validation);
                break;
            }

            // update
            if ($id) {
                $args['id'] = $id;
                $result = module_UpdateXxx($args);
            }

            // create
            if (empty($id)) {
                unset($args['id']);
                $result = module_AddXxx($args);
                $xxx = module_GetXxxById($result);
            }
            if ($result) {
                $success = "Xxx " . $xxx->getName() . " " . ($id ? 'updated' : 'created');
            }
            break;
        case 'delete':
            $result  = module_DeleteXxx([
                'centre_id' => getCentreId(),
                'id'        => $id
            ]);
            if ($result) {
                $success = "Xxx " . $xxx->getName() . " deleted";
            }
            break;
    }

    // action for save & delete
    if ($result) {
        $objResponse->script('DestroyDialog("pop_window")');
        $templateToUse = $listTemplate;
        $action = 'list';
    }

    // show success / error
    if (!is_null($result)) {
        $fieldType = ($div == 'details' ? '_msg' : '_pop_msg');
        if ($success) {
            $template->assign('success' . $fieldType, $success);
        }
        if ($error) {
            $template->assign('error' . $fieldType, $error);
        }
    }

    // search xxxs
    if ($action == 'list') {
        $searchArgs = [];
        if (!empty($args['srch_name'])) {
            $searchArgs['srch_name'] = $args['srch_name'];
            $template->assign('srch_name', $args['srch_name']);
        }
        if (!empty($args['srch_comment'])) {
            $searchArgs['srch_comment'] = $args['srch_comment'];
            $template->assign('srch_comment', $args['srch_comment']);
        }

        // Get xxxs
        $xxxs = api_call('module', 'GetXxxs', $searchArgs);
    }

    // assign variables
    $template->assign('xxxs', $xxxs);
    $template->assign('xxx', $xxx);
    $template->assign('user', $user);

    // handle output
    $objResponse->assign($div, 'innerHTML', $template->fetch($ajaxTemplatesPath . $templateToUse));
    $objResponse->script('initPublishTab();');
    $objResponse->script('initFormTabsBoth();');
    return $objResponse;
}
