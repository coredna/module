<?php

include_once $settings->_AppPath . $settings->_AdminDir . '/include/inc.AdminAPI.php';

global $user, $settings, $glb_centre, $template_name;

// setup the index.html
$modules_path = $settings->_AppPath . $settings->_AdminDir . '/modules';
$template_name = $modules_path . '/xxxs/templates/index.html';
$template->assign('modules_path', $modules_path);

/* protect for direct calls */
if ($file == '') {
    header('Location: index.php');
    exit;
}

$form_name = get_param('form_name');
$action = get_param('action');
list($x, $file) = explode('admin_', $action);
$module = $file;

if (get_param('authorize')) {
    // set the return module when the user is redirected to analytics
    $ganalytics = new CoreDNA_Google_Analytics();
    $ganalytics->authorizeClient();
    $url = $ganalytics->getAuthUrl();
    $_SESSION['return_module'] = 'abtesting';
    header('Location: ' . $url);
    die;
}

api_call('auth', 'init');
api_call('authprivate', 'init');
api_call('xajax', 'init');
api_call('api', 'init');
api_call('api', 'register_all_ajax_funcs', 'xxxs');
api_call('auth', 'finit');
api_call('xxxs', 'init');

$template->assign('form_name', $action);
$template->assign('id', get_param('id'));
