<?php

/**
 * Load module components and register class autoloader
 */
call_user_func(function () {
    // Load base stuff
    $includes = [
        'tables.php',
        'api.php',
        'api/xxx-api.php'
    ];
    array_walk($includes, function ($file) {
        require_once __DIR__ . DIRECTORY_SEPARATOR . $file;
    });
    // Register PSR-4 autoloader with the Slots namespace
    $classDir = __DIR__ . DIRECTORY_SEPARATOR . 'classes';
    CoreDNA\ModuleClassAutoloader::getInstance()->addNamespace('module_name', $classDir);
});

function xxx_init()
{
}
