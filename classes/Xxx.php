<?php

namespace module_name;

/**
 * Class Xxx
 */
class Xxx
{
    protected $id;
    protected $centre_id;
    protected $name;
    protected $publish;
    protected $comment;
    protected $created_at;
    protected $updated_at;

    public function __construct(array $dataArray = [])
    {
        if (!empty($dataArray)) {
            $this->setData($dataArray);
        }
    }

    /**
     * @param array $dataArray
     */
    public function setData(array $dataArray)
    {
        global $api_tables;
        $columns = $api_tables['xxxs_column'];
        foreach ($dataArray as $key => $data) {
            if (in_array($key, $columns) && !empty($key)) {
                $this->$key = $data;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Agent
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     * @return Agent
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return Agent
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     * @return Agent
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * @param mixed $publish
     * @return Agent
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Agent
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCentreId()
    {
        return $this->centre_id;
    }

    /**
     * @param mixed $centre_id
     * @return Agent
     */
    public function setCentreId($centre_id)
    {
        $this->centre_id = $centre_id;
        return $this;
    }
}
