<?php

global $api_tables;

$api_tables['xxxs'] = 'xxxs';
$api_tables['xxxs_column'] = [
    'id' => 'id',
    'centre_id' => 'centre_id',
    'name' => 'name',
    'publish' => 'publish',
    'comment' => 'comment',
    'created_at' => 'created_at',
    'updated_at' => 'updated_at'
];
