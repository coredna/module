<?php

/**
 * xxx's
 */

/**
 * Get Xxx from the database
 *
 * @param  array $args
 * @return array
 */
function module_userapi_get_xxxs($args = array())
{
    global $api_tables, $dbreader;

    $table = $api_tables['xxxs'];
    $column = $api_tables['xxxs_column'];

    // Get centre id
    $centreId = array_key_exists('centre_id', $args) ? intval($args['centre_id']) : $_SESSION['centre_id'];

    // Check for required args, abort if they are missing
    if (!intval($centreId)) {
        return false;
    }

    // Start selection, bind to centre id by default
    $sql = "WHERE $column[centre_id] = ?";
    $parameters = array(intval($centreId));

    // Process args
    foreach ($args as $columnKey => $parameter) {
        // Ignore pagination, ordering, grouping, centre_id
        if (in_array($columnKey, array('order_by', 'group_by', 'start', 'per_page', 'centre_id'))) {
            continue;
        }

        // Are we looking for null?
        if (is_null($parameter)) {
            $sql .= " AND $column[$columnKey] IS NULL";
            continue;
        }

        // Are we searching?
        if (substr($columnKey, 0, 5) == 'srch_') {
            $columnKey = substr($columnKey, 5);
            $sql .= " AND $column[$columnKey] LIKE ?";
            $parameters[] = '%' . $parameter . '%';
            continue;
        }

        // Are we looking for in/not in?
        if (substr($columnKey, -3) == '_in' && is_array($parameter)) {
            if (substr($columnKey, -7) == '_not_in') {
                $columnKey = substr($columnKey, 0, -7);
                $sql .= " AND $column[$columnKey] NOT IN (";
            } else {
                $columnKey = substr($columnKey, 0, -3);
                $sql .= " AND $column[$columnKey] IN (";
            }

            $qmarks = array();

            foreach ($parameter as $id) {
                $qmarks[] = '?';
                $parameters[] = $id;
            }

            $sql .= implode(',', $qmarks) . ')';
            continue;
        }

        // skip empty columns
        if (empty($column[$columnKey])) {
            continue;
        }

        // Normal select
        $sql .= " AND $column[$columnKey] = ?";
        $parameters[] = $parameter;
    }

    // Ordering
    if (array_key_exists('order_by', $args) && $args['order_by']) {
        $sql .= ' ORDER BY ' . prepForStore($args['order_by']);
    }

    // Grouping
    if (array_key_exists('group_by', $args) && $args['group_by']) {
        $sql .= ' GROUP BY ' . prepForStore($args['group_by']);
    }

    // Pagination
    if (array_key_exists('start', $args) && intval($args['start']) >= 0 && array_key_exists('per_page', $args) && intval($args['per_page']) > 0) {
        // Ordering doesn't matter for counting but grouping does
        $grouping = (array_key_exists('group_by', $args) && $args['group_by']) ? ' GROUP BY ' . prepForStore($args['group_by']) : '';

        $total = $dbreader->getOne("SELECT count(*) FROM $table " . $sql . $grouping, $parameters);

        $sql .= ' LIMIT ?,?';
        $parameters[] = intval($args['start']);
        $parameters[] = intval($args['per_page']);
    }

    $results = $dbreader->getAll("SELECT * FROM $table " . $sql, $parameters);

    // If we didn't get any results, abort
    if (!count($results)) {
        // Using pagination
        if (isset($total)) {
            return array('total' => $total, 'output' => $results);
        }

        return $results;
    }

    // Paginated
    if (isset($total)) {
        return array('total' => $total, 'output' => $results);
    }

    return $results;
}

/**
 * Add a single xxx to the DB
 *
 * @param  array $args
 * @return mixed
 */
function module_userapi_add_xxx(array $args = array())
{
    global $api_tables, $dbwriter;

    $table = $api_tables['xxxs'];
    $column = $api_tables['xxxs_column'];

    // Get centre id
    $centreId = array_key_exists('centre_id', $args) ? intval($args['centre_id']) : $_SESSION['centre_id'];

    // Check for required args, abort if they are missing
    if (!arrayGet($args, 'xxx') || !intval($centreId)) {
        return false;
    }

    $sql = "INSERT INTO $table";

    // Place holders for binding information, always bind insert to centre id by default
    $columns = array($column['centre_id']);
    $parameters = array(intval($centreId));
    $qmarks = array('?');

    // Add each arg passed
    foreach ($args as $columnKey => $parameter) {
        // Ignore nulls and centre_id
        if (is_null($parameter) || $columnKey == 'centre_id' || $columnKey == 'id' || empty($column[$columnKey])) {
            continue;
        }

        $columns[] = $column[$columnKey];
        $parameters[] = $parameter;
        $qmarks[] = '?';
    }

    // Set created & updated at stamp
    if (!arrayGet($parameters, 'created')) {
        $columns[] = $column['created'];
        $parameters[] = date('Y-m-d H:i:s');
        $qmarks[] = '?';
    }
    if (!arrayGet($parameters, 'updated')) {
        $columns[] = $column['updated'];
        $parameters[] = date('Y-m-d H:i:s');
        $qmarks[] = '?';
    }

    $sql .= '(' . implode(',', $columns) . ') VALUES (' . implode(',', $qmarks) . ')';

    $result = $dbwriter->execute($sql, $parameters);

    // Return insert id if successful
    if ($result) {
        return $dbwriter->Insert_ID();
    }

    // Return false
    return $result;
}

/**
 * Update an existing xxx
 *
 * @param  array $args
 * @return boolean
 */
function module_userapi_update_xxx($args = array())
{
    global $api_tables, $dbwriter;

    $table = $api_tables['xxxs'];
    $column = $api_tables['xxxs_column'];

    // Fetch centre id
    $centreId = array_key_exists('centre_id', $args) ? intval($args['centre_id']) : $_SESSION['centre_id'];

    // We must have an id and centre id
    if (!intval(arrayGet($args, 'id')) || !intval($centreId)) {
        return false;
    }

    $sql = "UPDATE $table SET";

    foreach ($args as $columnKey => $parameter) {
        // Ignore nulls and id
        if (is_null($parameter) || $columnKey == 'id' || empty($column[$columnKey])) {
            continue;
        }

        $sql .= " $column[$columnKey] = ?,";
        $parameters[] = $parameter;
    }

    // If we have no parameters there is nothing to do
    if (!count($parameters)) {
        return false;
    }

    // Set update stamp
    if (!arrayGet($parameters, 'updated')) {
        $sql .= " $column[updated] = ?";
        $parameters[] = date('Y-m-d H:i:s');
    }

    // Trim any trailing commas
    $sql = trim($sql, ',') . " WHERE $column[centre_id] = ? AND $column[id] = ?";

    // Bind centre and record id
    $parameters[] = intval($centreId);
    $parameters[] = intval($args['id']);

    return $dbwriter->execute($sql, $parameters);
}

/**
 * Delete a single xxx from the DB
 *
 * @param  array $args
 * @return boolean
 */
function module_userapi_delete_xxx(array $args = array())
{
    global $api_tables, $dbwriter;

    $table = $api_tables['xxxs'];
    $column = $api_tables['xxxs_column'];

    // Get centre id
    $centreId = array_key_exists('centre_id', $args) ? intval($args['centre_id']) : $_SESSION['centre_id'];

    // We must have an id and centre id
    if (!intval(arrayGet($args, 'id')) || !intval($centreId)) {
        return false;
    }

    // Set up SQL
    $sql = "DELETE FROM $table WHERE $column[centre_id] = ? AND $column[id] = ?";

    // Bind parameters
    $parameters = array(
        intval($centreId),
        intval($args['id']),
    );

    // Return true/false
    return $dbwriter->execute($sql, $parameters);
}
