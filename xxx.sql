CREATE TABLE IF NOT EXISTS `xxxs` (
  `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `centre_id`  INT(10) UNSIGNED NOT NULL,
  `name`       VARCHAR(255)     NOT NULL,
  `publish`    TINYINT(1)       NOT NULL DEFAULT '0',
  `comment`    TEXT             NULL,
  `created_at` TIMESTAMP        NOT NULL,
  `updated_at` TIMESTAMP        NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;