This is a default barebones module which should help in the creation of new module's.

Anywhere you see xxx, replace it with your module name.

Steps:

1.  Rename 'xxx' to whatever your module is called, (all lowercase) in every file. This handles the function methods.
    Case sensitivity is very important.
2.  Rename 'Xxx' to whatever your module is called, (uppercase first letter) in every file. This handles the class names.
    Case sensitivity is very important.
3.  Run the xxx.sql to create the basic table.
4.  Replace any occurance of 'module_' with the name of this module.
5.  Place module into coredna11/modules/
6.  With the DB created, files renamed correctly and module in place, navigate to admin_modulename and you should be ready to start
7.  Delete xxx.sql and this readme.
